package ic.android.ui.view.maskedcodefield


import android.content.Context
import android.graphics.Typeface
import android.text.InputType
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.KeyEvent
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.widget.LinearLayout
import ic.android.storage.res.getResFont

import ic.android.ui.view.ext.requestFocusAndKeyboard
import ic.android.ui.view.ext.setOnClickAction
import ic.android.ui.view.ext.setOnLongClickAction
import ic.android.ui.view.text.TextView
import ic.android.ui.view.text.ext.textColorArgb
import ic.android.ui.view.text.ext.value
import ic.android.ui.view.group.ext.get
import ic.android.util.clipboard.copyToClipboard
import ic.android.util.clipboard.getStringFromClipboard
import ic.android.ui.popup.contextmenu.openContextMenu
import ic.base.strings.ext.isNotEmpty
import ic.base.primitives.character.asString
import ic.base.primitives.int32.ext.asFloat32
import ic.base.throwables.WrongValueException
import ic.graphics.color.ColorArgb

import ic.gui.maskedcodefield.R


class MaskedCodeField : LinearLayout {


	init {
		orientation = HORIZONTAL
		isClickable = true
		isFocusable = true
	}


	private var maxLengthField : Int = 0
	var maxLength : Int
		get() = maxLengthField
		set(value) {
			maxLengthField = value
			updateStructure()
		}
	;

	private fun updateStructure() {
		removeAllViews()
		for (i in 0 until maxLengthField) {
			addView(
				TextView(context).apply {
					gravity = Gravity.CENTER
				}
			)
		}
		updateDimensions()
		updateTextSize()
		updateTextFont()
		updateValue()
	}


	private var groupLengthField : Int = 1
	var groupLength : Int
		get() = groupLengthField
		set(value) {
			groupLengthField = value
			updateDimensions()
		}
	;

	private var charWidthPxField : Int = 0
	var charWidthPx : Int
		get() = charWidthPxField
		set(value) {
			charWidthPxField = value
			updateDimensions()
		}
	;

	private var groupDividerWidthPxField : Int = 0
	var groupDividerWidthPx : Int
		get() = groupDividerWidthPxField
		set(value) {
			groupDividerWidthPxField = value
			updateDimensions()
		}
	;

	private var hintPlaceholderCharacterField : String = "●"
	var hintPlaceholderCharacter : String
		get() = hintPlaceholderCharacterField
		set(value) {
			hintPlaceholderCharacterField = value
			updateValue()
		}
	;

	private fun updateDimensions() {
		for (i in 0 until maxLengthField) {
			val charView : TextView = this[i]
			charView.layoutParams = (charView.layoutParams as LinearLayout.LayoutParams).apply {
				width = charWidthPxField
				height = MATCH_PARENT
				leftMargin = (
					if (i % groupLengthField == 0 && i != 0) {
						groupDividerWidthPxField
					} else {
						0
					}
				)
			}
		}
	}


	private var textSizePxField : Int = 0
	var textSizePx : Int
		get() = textSizePxField
		set(value) {
			textSizePxField = value
			updateTextSize()
		}
	;

	private fun updateTextSize() {
		for (i in 0 until maxLengthField) {
			val charView : TextView = this[i]
			charView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizePxField.asFloat32)
		}
	}


	private var textFontField : Typeface = Typeface.DEFAULT
	var textFont : Typeface
		get() = textFontField
		set(value) {
			textFontField = value
			updateTextFont()
		}
	;

	private fun updateTextFont() {
		for (i in 0 until maxLengthField) {
			val charView : TextView = this[i]
			charView.typeface = textFontField
		}
	}


	private var textColorField : ColorArgb = 0
	var textColorArgb : ColorArgb
		get() = textColorField
		set(value) {
			textColorField = value
			updateValue()
		}
	;

	private var hintColorField : ColorArgb = 0
	var hintColor : ColorArgb
		get() = hintColorField
		set(value) {
			hintColorField = value
			updateValue()
		}
	;


	private var valueField : String = ""
	var value : String
		get() = valueField
		set (value) {
			val oldValue = valueField
			val filteredValue = textFilter(oldValue, value)
			valueField = filteredValue
			updateValue()
			onValueChangedAction(oldValue, filteredValue, isInvokedByUserInteraction)
			isInvokedByUserInteraction = false
		}
	;

	private var onValueChangedAction
		: (oldValue: String, newValue: String, isInvokedByUserInteraction: Boolean) -> Unit
		= { _, _, _ -> }
	;

	private var isInvokedByUserInteraction : Boolean = false

	fun setOnValueChangedAction (
		toCallAtOnce : Boolean = false,
		action : (oldValue: String, newValue: String, isInvokedByUserInteraction: Boolean) -> Unit
	) {
		this.onValueChangedAction = action
		if (toCallAtOnce) action(valueField, valueField, false)
	}

	private fun updateValue() {
		for (i in 0 until maxLengthField) {
			val charView : TextView = this[i]
			charView.value = (
				if (i < valueField.length) {
					charView.textColorArgb = textColorField
					valueField[i].asString
				} else {
					charView.textColorArgb = hintColorField
					hintPlaceholderCharacterField
				}
			)
		}
	}


	var textType : TextType = TextType.LatinLettersAndDigits


	var isLocked : Boolean = false


	init {
		isClickable = true
		isFocusable = true
		isFocusableInTouchMode = true
		isLongClickable = true
		setOnClickAction {
			if (!isLocked) {
				requestFocusAndKeyboard()
			}
		}
		setOnLongClickAction {
			if (!isLocked) {
				openContextMenu(
					ic.android.ui.popup.contextmenu.ContextMenuItem(android.R.string.copy) {
						copyToClipboard(valueField)
					},
					ic.android.ui.popup.contextmenu.ContextMenuItem(android.R.string.paste) {
						var stringFromClipboard = getStringFromClipboard() ?: ""
						if (stringFromClipboard.length > maxLengthField) {
							stringFromClipboard =
								stringFromClipboard.substring(0, maxLengthField)
						}
						isInvokedByUserInteraction = true
						value = stringFromClipboard
					}
				)
			}
		}
	}

	override fun onCreateInputConnection(outAttrs: EditorInfo) : InputConnection? {
		outAttrs.inputType = when (textType) {
			TextType.LatinLettersAndDigits -> (
				InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
			)
			TextType.DigitsOnly -> InputType.TYPE_CLASS_NUMBER
		}
		outAttrs.imeOptions = EditorInfo.IME_ACTION_DONE
		return null
	}


	override fun dispatchKeyEvent (event: KeyEvent) : Boolean {
		if (event.action == KeyEvent.ACTION_DOWN) {
			if (!isLocked) {
				when {
					event.keyCode == KeyEvent.KEYCODE_DEL -> {
						if (value.isNotEmpty) {
							isInvokedByUserInteraction = true
							value = value.substring(0, value.length - 1)
						}
					}
					else -> {
						if (value.length < maxLengthField) {
							val c = event.unicodeChar.toChar()
							if (textType.filterChar(c)) {
								isInvokedByUserInteraction = true
								value += c
							}
						}
					}
				}
			}
		}
		return super.dispatchKeyEvent(event)
	}


	private var textFilter : (oldValue: String, newValue: String) -> String = { _, newValue -> newValue }

	fun setTextFilter (textFilter: (oldValue: String, newValue: String) -> String) {
		this.textFilter = textFilter
		value = valueField
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super (context, attrs, defStyleAttr) {

		if (attrs != null) {

			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.MaskedCodeField)

			maxLengthField = styledAttributes.getInteger(R.styleable.MaskedCodeField_maxLength, maxLengthField)

			groupLengthField = styledAttributes.getInteger(R.styleable.MaskedCodeField_groupLength, groupLengthField)

			charWidthPxField = styledAttributes.getDimensionPixelSize(R.styleable.MaskedCodeField_charWidth, charWidthPxField)

			groupDividerWidthPxField = (
				styledAttributes.getDimensionPixelSize(R.styleable.MaskedCodeField_groupDividerWidth, groupDividerWidthPxField)
			)

			textSizePxField = styledAttributes.getDimensionPixelSize(R.styleable.MaskedCodeField_textSize, textSizePxField)

			val textFontName = styledAttributes.getString(R.styleable.MaskedCodeField_textFontName)
			if (textFontName != null) {
				textFontField = getResFont(fontResName = textFontName)
			}

			textColorField = styledAttributes.getColor(R.styleable.MaskedCodeField_textColor, textColorField)
			hintColorField = styledAttributes.getColor(R.styleable.MaskedCodeField_hintColor, hintColorField)

			valueField = styledAttributes.getString(R.styleable.MaskedCodeField_value) ?: valueField

			hintPlaceholderCharacterField = (
				styledAttributes.getString(R.styleable.MaskedCodeField_hintPlaceholderCharacter) ?:
				hintPlaceholderCharacterField
			)

			isLocked = styledAttributes.getBoolean(R.styleable.MaskedCodeField_isLocked, isLocked)

			val textTypeCode = styledAttributes.getInteger(R.styleable.MaskedCodeField_textType, 0)
			if (textTypeCode != 0) {
				textType = when (textTypeCode) {
					1 -> TextType.LatinLettersAndDigits
					2 -> TextType.DigitsOnly
					else -> throw WrongValueException.Error("textTypeCode: $textTypeCode")
				}
			}

			styledAttributes.recycle()

		}

		updateStructure()

	}


}
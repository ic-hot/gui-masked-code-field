package ic.android.ui.view.maskedcodefield


import ic.base.primitives.character.Character
import ic.base.primitives.character.ext.isDigit
import ic.base.primitives.character.ext.isLatinLetterOrDigit


sealed class TextType {

	abstract fun filterChar (c: Character) : Boolean

	object LatinLettersAndDigits : TextType() {
		override fun filterChar (c: Character) = c.isLatinLetterOrDigit
	}

	object DigitsOnly : TextType() {
		override fun filterChar (c: Character) = c.isDigit
	}

}